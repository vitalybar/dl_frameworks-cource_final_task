import polars as pl
import os
import pickle
import mlflow
import click

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error


@click.command()
@click.argument("experiment_name", type=str)
@click.argument("run_name", type=str)
@click.argument("processed_features_csv", type=click.Path(exists=True))
@click.argument("processed_labels_csv", type=click.Path(exists=True))
@click.argument("n_estimators", type=int)
@click.argument("max_depth", type=int)
@click.argument("max_features", type=int)
@click.argument("test_size", type=float)
def model_train(
        experiment_name: str,
        run_name: str,
        processed_features_csv: str,
        processed_labels_csv: str,
        n_estimators: int,
        max_depth: int,
        max_features: float,
        test_size: float,
        random_state: int = 4221
):
    """
    Random forest model training method\n
    :param X_train: features dataframe
    :param y_train: labels dataframe
    :param n_estimators: number of trees in the forest
    :param max_depth: maximum depth of the tree
    :param max_features: number of features to consider when looking
     for the best split
    :param random_state: controls both the randomness of the bootstrapping
     of the samples used when building trees
    :return: trained Random Forest model
    """
    mlflow.set_experiment(experiment_name=experiment_name)

    mlflow.sklearn.autolog()

    with mlflow.start_run(run_name=run_name) as run:

        processed_features = pl.read_csv(processed_features_csv).to_pandas()
        processed_labels = pl.read_csv(processed_labels_csv).to_pandas()

        params = {
            "n_estimators": n_estimators,
            "max_depth": max_depth,
            "max_features": max_features,
            "test_size": test_size
        }

        X_train, X_test, y_train, y_test = train_test_split(
            processed_features,
            processed_labels,
            test_size=params["test_size"],
            random_state=random_state
        )

        model = RandomForestRegressor(n_estimators=params["n_estimators"],
                                      max_depth=params["max_depth"],
                                      max_features=params["max_features"],
                                      random_state=random_state)
        model.fit(X_train, y_train)

        prediction = model.predict(X_test)

        metrics = {
            "n_estimators": params["n_estimators"],
            "mae": mean_absolute_error(y_test, prediction),
            "mse": mean_squared_error(y_test, prediction),
            "r2_score": r2_score(y_test, prediction),
            "rms": mean_squared_error(y_test, prediction, squared=False),
        }

        metrics_df = pl.from_dict(metrics)
        metrics_df.write_csv(f"models/{run_name}_metrics.csv")

        with open(f"models/{run_name}_model.pkl", mode="wb") as random_forest:
            pickle.dump(model, random_forest)

        print("Training was successful")

        mlflow.log_metric("n_estimators", params["n_estimators"])

    # return model
    # mlflow.log_params(params)
    # mlflow.log_metrics(score)
    # mlflow.log_artifacts("models")


if __name__ == "__main__":
    # os.chdir("../../")
    #
    # params = {}
    # with open("optuna_best_trials/best_params_0.txt", "r") as outfile:
    #     params = json.load(outfile)
    #
    # with open('spec_params.yaml', 'r') as yaml_file:
    #     params = yaml.safe_load(yaml_file)["models"]
    #
    # print(params)
    model_train()
