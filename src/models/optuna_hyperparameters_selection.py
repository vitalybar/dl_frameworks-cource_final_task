import json
import os

import click
import polars as pl
import pickle
import optuna
import yaml

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score

experiments = []
random_state = 4221


def model_train(
        X_train: pl.DataFrame,
        y_train: pl.DataFrame,
        n_estimators: int,
        max_depth: int = None,
        max_features: float = 1.0,
        random_state: int = 4221
):
    """
    Random forest model training method\n
    :param X_train: features dataframe
    :param y_train: labels dataframe
    :param n_estimators: number of trees in the forest
    :param max_depth: maximum depth of the tree
    :param max_features: number of features to consider when looking
     for the best split
    :param random_state: controls both the randomness of the bootstrapping
     of the samples used when building trees
    :return: trained Random Forest model
    """
    model = RandomForestRegressor(n_estimators=n_estimators,
                                  max_depth=max_depth,
                                  max_features=max_features,
                                  random_state=random_state)

    model.fit(X_train, y_train)

    return model


def save_model(save_model_path: str, params: dict):
    """
    Save model function
    :param save_model_path: path to saving model
    :param params: params dict
    :return: None
    """
    X_train, _, y_train, _ = train_test_split(
        processed_features,
        labels,
        test_size=params["test_size"],
        random_state=random_state
    )

    # train model with best params
    model = model_train(
        X_train,
        y_train,
        n_estimators=params["n_estimators"],
        max_depth=params["max_depth"],
        max_features=params["max_features"],
        random_state=random_state
    )

    # save rf_model
    with open(save_model_path, mode="wb") as random_forest:
        pickle.dump(model, random_forest)


def objective(trial: optuna.Trial):
    """
    Optuna trial function
    :param trial: optuna.Trial object
    :return: accuracy_score, parameters dictionary of trial
    """
    params = {
        "number": trial.number,
        "n_estimators": trial.suggest_int("n_estimators", 10, 1000),
        "max_depth": trial.suggest_int("max_depth", 10, 1000),
        "max_features": trial.suggest_int("max_features", 100, 1000),
        "test_size": trial.suggest_float("test_size", 0.2, 0.5),
        # "random_state": 4221,
        # "score": 0,
    }

    X_train, X_test, y_train, y_test = train_test_split(
        processed_features,
        labels,
        test_size=params["test_size"],
        random_state=random_state
    )

    # train and eval metrics
    prediction = model_train(
        X_train,
        y_train,
        n_estimators=params["n_estimators"],
        max_depth=params["max_depth"],
        max_features=params["max_features"],
        random_state=random_state
    ).predict(X_test)

    # params["score"] = r2_score(y_test, prediction)

    # experiments.append(params)

    return r2_score(y_test, prediction), params["n_estimators"]

@click.command()
@click.argument("optuna_trials_count", type=int)
@click.argument("optuna_best_trials_count", type=int)
def optuna_trials(optuna_trials_count: int,
                  optuna_best_trials_count: int):

    storage = "postgresql://root:root@localhost:5432/optuna_db"
    path_to_trials_data = "log/trials_log.csv"

    # Start optuna study with maximizing accuracy and minimizing n_estimators
    study = optuna.create_study(
        study_name="random_forest_study",
        directions=["maximize", "minimize"],
        storage=storage,
        load_if_exists=True,
    )
    study.set_metric_names(["R2_score", "n_estimators"])
    study.optimize(objective, n_trials=optuna_trials_count)

    df = study.trials_dataframe(attrs=("number", "value", "params", "state"))

    # print(df)
    # Save trials data
    df.to_csv(path_to_trials_data)

    # Save best trials params
    best_models_params_list = []
    for counter in range(optuna_best_trials_count):
        params = study.best_trials[counter].params
        params["name"] = f"optuna_best_params_{counter}"
        best_models_params_list.append(params)
    best_models_params = {"best_model_params": best_models_params_list}

    with open(f"optuna_best_trials/best_model_params.yaml", "w") as outfile:
        yaml.dump(best_models_params, outfile)

if __name__ == "__main__":

    # os.chdir('../../')

    processed_features = pl.scan_csv("data/processed/processed_features.csv") \
        .collect()
    labels = pl.scan_csv("data/processed/processed_labels.csv").collect()

    optuna_trials()

