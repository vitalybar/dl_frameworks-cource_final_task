import os
import polars as pl


def replace_none(df: pl.LazyFrame):
    """
    The method replaces none values in numeric attributes\n
    :param df: features lazyframe
    :return: replaced none lazyframe
    """
    replaced_none_columns_df = df
    for counter, column in enumerate(df.columns):
        if df.dtypes[counter] is not pl.Utf8:
            mean_value = \
                replaced_none_columns_df.select(pl.mean(column)) \
                .collect()[column][0]
            replaced_none_columns_df = replaced_none_columns_df.with_columns(
                pl.col(column).cast(pl.Float64).fill_null(mean_value)
                # Можно убрать, но тогда все значения в датасете будут Float64
                # .cast(pl.Int64)
            )
    return replaced_none_columns_df


def count_encoding(df: pl.LazyFrame):
    """
    The method produces count encoding with categorical attributes\n
    :param df: features lazyframe
    :return: count encoded lazyframe
    """
    count_encoded_data = df
    for counter, column in enumerate(df.columns):
        if (df.dtypes[counter] is pl.Utf8) and (column != "MathScore"):
            count_encoded_column = df.group_by(column).agg(
                pl.count().alias(f"{column}_encoded"))
            count_encoded_data = count_encoded_data.join(count_encoded_column,
                                                         on=column,
                                                         how="left").drop(
                column)
    return count_encoded_data


def z_score_processing(df: pl.LazyFrame):
    """
    The method preparing and processing features\n
    :param df: features lazyframe
    :return: z-processed lazyframe
    """
    z_scored_df = df
    mean_values = z_scored_df.mean()
    stddev_values = z_scored_df.std()
    for counter, column in enumerate(df.columns):
        if (df.dtypes[counter] is not pl.Utf8) and (column != "MathScore"):
            mean = mean_values.select(column).collect()[column][0]
            stddev = stddev_values.select(column).collect()[column][0]
            z_scored_df = z_scored_df.with_columns(
                ((pl.col(column) - mean / stddev).alias(f"{column}_z-score"))
            )
            z_scored_df = z_scored_df.drop(column)
    return z_scored_df


def outliers_removing(df: pl.LazyFrame):
    """
    The method removes statistics outliers beyond 5 and 95 percentiles\n
    :param df: features lazyframe
    :return: outliers removed lazyframe
    """
    outliers_removing = df
    for counter, column in enumerate(df.columns):
        if (df.dtypes[counter] is not pl.Utf8) and (column != "MathScore"):
            percentile_5 = outliers_removing.select(column).quantile(0.05) \
                .collect()[column][0]
            percentile_95 = outliers_removing.select(column).quantile(0.95) \
                .collect()[column][0]
            # print(column, percentile_5, percentile_95)
            outliers_removing = outliers_removing.filter(
                (pl.col(column) >= percentile_5)
                & (pl.col(column) <= percentile_95)
            )
    return outliers_removing


def data_processing(
        features_csv: str,
        processed_labels_csv: str,
        processed_features_csv: str
):
    """
    The method preparing and processing features:\n
    - Converts categorical values with Count Encoding
    - Replaces null values with average values
    - Converts all numeric features with Z-score
    - Removes noisy values beyond 5 and 95 percentile
    :param features_csv: path to features csv file
    :param processed_labels_csv: path to processed labels csv file
    :param processed_features_csv: path to processed features csv file
    :return: created processed_features.csv
    """
    raw_features = pl.scan_csv(features_csv)
    # print(df.select(pl.count()).collect())
    # Удаляю none значения из числовых признаков
    raw_features = replace_none(raw_features)
    # Удаляю выбросы у числовых признаков
    raw_features = outliers_removing(raw_features)
    # Нормализую все числовые признаки с Z-score
    raw_features = z_score_processing(raw_features)
    # Произвожу Count Encoding для всех категориальных признаков
    raw_features = count_encoding(raw_features)

    labels = raw_features.select("MathScore")
    raw_features = raw_features.drop("MathScore")

    labels.collect().write_csv(processed_labels_csv)
    raw_features.collect().write_csv(processed_features_csv)

    print("Features processed successfully")


if __name__ == "__main__":
    # os.chdir('../../')
    data_processing(
        "data/interim/raw_data.csv",
        "data/processed/processed_labels.csv",
        "data/processed/processed_features.csv"
    )
