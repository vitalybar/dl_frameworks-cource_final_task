import os
import polars as pl


def feature_preparing(
        raw_data_csv: str,
        feature_csv: str,
        labels_csv: str
):
    """
    The method receives and performs data preprocessing
    :param str raw_data_csv: path to raw data file
    :param str feature_csv: path to prepared features
    :param str labels_csv: path to prepared labels
    :return: created features.csv and labels.csv
    """
    raw_data = pl.scan_csv(raw_data_csv)

    data = raw_data.drop([""])
    labels = raw_data.select("MathScore")

    data.collect().write_csv(feature_csv)
    labels.collect().write_csv(labels_csv)

    print("Data extracted successfully")


if __name__ == "__main__":
    # os.chdir('../../')
    feature_preparing(
        "data/raw/edu.csv",
        "data/interim/raw_data.csv",
        "data/interim/labels.csv"
    )
