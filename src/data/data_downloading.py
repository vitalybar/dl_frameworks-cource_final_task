import os
import wget


def data_downloading(data_dir_url: str, files: list[str]):
    """
    The method downloading data files
    :param data_dir_url: URL to data dir
    :param files: files to downloading
    :return: create features.csv and labels.csv
    """

    for file in files:
        if not os.path.isfile(f"data/raw/{file}"):
            print(f"Downloading {file}")
            wget.download(data_dir_url + file, f"data/raw/{file}")


if __name__ == "__main__":
    # os.chdir('../../')
    data_downloading(
        "https://raw.githubusercontent.com/kudep/itmo-dl-frameworks-last-task"
        "/dev/datasets/",
        ["about_edu_dataset.md", "edu.csv"]
    )
