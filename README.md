dl_frameworks-cource_final_task
==============================

Задание на комиссию по дисциплине "Разработка приложений разговорного ИИ".

Для проверки задания необходимо поднять докер контейнер с праметрами,
прописанными в `docker/docker-compose.yml` перейдя в директорию `docker`, и выполнив:
```bash
docker compose up
```
Примечание: все переменные лежат в файле `.env`

Установка завсимостей и запуск кода осуществляется следующими командами из корня проекта:
```bash
pip install -r requirements.txt
dvc repro
mlflow ui
```
Примечание: перед выполнением необходимо убрать из отслеживания git файл `best_model_params.yaml`
выполнив команды, так как он находится в зависимостях у DVC: 
```bash
git rm -r --cached 'optuna_best_trials\best_model_params.yaml'
```
### Задача
Поставлена задача регрессии MathScore в предложенном датасете "Успеваемость студентов".
Решающая модель Random Forest (scikit-learn).

## Этапы
### Обработка данных
На этапе обработки данных все датасеты были обработаны с использованием только 
lazy API из библиотеки Polars. Были выполнены следующие этапы: 
 
- Преобразование всех категориальных признаков с использованием Count Encoding.
- Замена значений "none" средними значениями (без удаления соответствующих столбцов).
- Преобразование всех числовых признаков с использованием Z-score.
- Удаление шумных значений, находящихся за пределами 5 и 95 перцентилей (для Random Forest).

Результатами данных этапов являются представленные в данном проекте CSV-файлы в директории
data, и скрипты в src/data.

- `src/data/data_downloading.py` - загрузка данных
- `src/data/featuring_extracting.py` - извлечение данных
- `src/data/data_processing.py` - обработка данных

В файле `dvc.yaml` этот этап представлен стейджами `data_processing`, `data_extracting`, `data_downloading`
### Выбор признаков
На этапе подбора признаков модели была использована Optuna.

Для ее работы была поднята база данных PostgreSQL в докер контейнере, для
сохранения данных Optuna. `docker-compose.yml` и соответствующий ему .env 
расположены в директории docker.

Optuna при подборе параметров создает CSV-файл с параметрами и признаками, которые
регистрировались в ходе подбора, которых располагается в директории `log`.

Результат этапа:
- Скрипт `src/models/optuna_hyperparameters_selection.py`
- Файл `optuna_best_trials/best_model_params.yaml`, содержащий подобранные параметры

В файле `dvc.yaml` этот этап представлен стейджем `optuna_hyperparameters_selection`
### Обучение модели
На этом этапе происходит тренировка моделей с параметрами, выбранными на предыдущем этапе,
с использованием MLFlow. 

Параметры стадии определены в файле, который записывается на предыдущем этапе
`best_model_params.yaml`, загрузка которых происходит через переменные в `dvc.yaml`.

В задании было требование к использованию `Matrix` для загрузки параметров в стадию обучения.

Параметризация стадии:
- Все параметры стадии подробно описаны в файле `optuna_best_trials/best_model_params.yaml`.
    - Для Random Forest предусмотрены списки с различными значениями `n_estimators`, 
  `max_depth` и другими параметрами.
- Загрузка всех параметров осуществляется через `vars` в файле `dvc.yaml` с использованием `best_model_params.yaml`.
- Загруженные параметры из файла `best_model_params.yaml` передаются в стадию обучения (`train`) с использованием:
    - `matrix` для Random Forest.

В файле `dvc.yaml` этот этап представлен стейджем `model_train`

Project Organization
------------

    ├── LICENSE
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   ├── data_downloading.py
    │   │   ├── data_processing.py
    │   │   └── featuring_extracting.py
    │   │
    │   ├── models         <- Scripts to select hyperparameters by optuna and train models
    │   │   ├── optuna_hyperparameters_selection.py
    │   │   └── train_model.py
    │
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------
